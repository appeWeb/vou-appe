<?php 

require_once('config/login_config.php');
require_once('src/google_auth.php');

$client = new Google_Client();
$client->setApplicationName("Google UserInfo PHP Starter Application");
$oauth2 = new Google_Oauth2Service($client);

$user = $facebook->getUser();
// IF FACEBOOK
if ($user) { 
  try { 
     $user_profile = $facebook->api('/me?fields=id,name,email,gender,locale,picture');
	 $logoutUrl = $facebook->getLogoutUrl();
	 $_SESSION['oauth_type'] ="facebook";
     } 
	 
	 catch (FacebookApiException $e) {
     $user = null;
  }
} 
// NOT FACEBOOK
else { 

  $loginUrl = $facebook->getLoginUrl(array('scope' => 'email'));
  // IF GOOGLE
  if (isset($_GET['code'])) { 
    $client->authenticate($_GET['code']);
    $_SESSION['token'] = $client->getAccessToken();
    $_SESSION['oauth_type'] ="google";
    $user_profile = $oauth2->userinfo->get();
  }
  // NOT GOOGLE
  else { 
    $authUrl = $client->createAuthUrl(); 
  }  
}

///------------LogOut Auth --------------//////
if (isset($_REQUEST['logout'])) {
  unset($_SESSION['token']);
  unset($_SESSION["U_NAME"]);
  unset($_SESSION["A_USERNAME"]);
  unset($_SESSION["P_USERID"]);
  session_destroy();
  if ($user) {
    header("Location:".$logoutUrl);
  }
  else {
    $client->revokeToken();
    header('Location: '.$_SERVER['PHP_SELF']); 
  }
}
?>