<?php

include_once("config/login_config.php");
if(array_key_exists('logout',$_GET))
{
	unset($_SESSION['token']);
	unset($_SESSION["user_data"]);
	session_destroy();
	if ($user) {
		header("Location: ".$logoutUrl);
		exit();
	}
	else if($client){
		$client->revokeToken();
	}
	exit(header('Location: index.php')); 
}

?>