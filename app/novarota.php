<?php
session_start();
//redirect to login
if(!isset($_SESSION['user_data']['name']))
  header('Location: login.php'); 
if(isset($_SESSION['route'])){
  header('Location: criandorota.php'); 
}

// for bd acess
require_once('config/db_op.class.php');
ini_set('default_charset','UTF-8');

?>

<!doctype html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Appé</title>

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">

    <!-- Web Application Manifest -->
    <link rel="manifest" href="manifest.json">

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="Appé">
    <link rel="icon" sizes="192x192" href="images/touch/chrome-touch-icon-192x192.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Appé">
    <link rel="apple-touch-icon" href="images/touch/apple-touch-icon.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#EFCD62">

    <!-- Material Design icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Your styles -->
    <link rel="stylesheet" href="styles/main.css">

    <!-- Jquery -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
  </head>
   <body class="route-page is-footer-fixed">
    <header class="app-header">
      <a href="/" class="app-header-menu js-lateral-menu mdl-button mdl-js-button mdl-button--icon">
        <i class="material-icons">arrow_back</i>
      </a>
      <span class="app-header-title">Nova rota</span>
    </header>
    <div class="route-explain app-intro">
      <img class="route-intro-illustration app-intro-illustration" src="images/illustrations/route_graph.svg" />
      <div class="route-explain-content app-intro-content">
        <span class="route-explain-title app-intro-title">Criar uma rota é tão simples quanto andar.</span>
        <span class="route-explain-subtitle app-intro-subtitle">Preencha as informações abaixo e ande até o seu destino. Sim, só isso mesmo.</span>
      </div>
    </div>
    <section class="route-body content-body">
      <h2>Quais os destinos dessa rota?</h2>
      <form enctype="multipart/form-data" action="config/send_route.php" accept-charset="utf-8" method="post">
       
        <label class="route-input route-input-start" for="inicio">
            <input required autofocus maxlength="30" type="text" name="inicio" id="inicio" placeholder="Essa rota parte de:">
        </label>
        <label class="route-input route-input-end" for="fim">
          <input required type="text" maxlength="30" name="fim" id="fim" placeholder="Até chegar em:">
        </label>
        
        <h2>Eu considero essa rota:</h2>
        <span class="subtitle">Escolha duas categorias.</span>

        <input type="checkbox" id="arborizada" name="char[]" value="arborizada">
        <label class="route-select" for="arborizada">
          <span class="route-select-icon"></span>Verde
        </label>

        <input type="checkbox" id="segura" name="char[]" value="segura">
        <label class="route-select" for="segura">
          <span class="route-select-icon"></span>Segura
        </label>

        <input type="checkbox" id="acessivel" name="char[]" value="acessivel">
        <label class="route-select" for="acessivel">
          <span class="route-select-icon"></span>Acessível
        </label>

        <input type="checkbox" id="turistica" name="char[]" value="turistica">
        <label class="route-select" for="turistica">
          <span class="route-select-icon"></span>Turística
        </label>

        <input type="checkbox" id="movimentada" name="char[]" value="movimentada">
        <label class="route-select" for="movimentada">
          <span class="route-select-icon"></span>Povoada
        </label>
        <footer class="route-footer">
          <button id="submit-btn" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary">Começar gravação</button>
        </footer>
      </form>
      
    </section> 
   <!-- build:js(app/) ../../scripts/main.min.js -->
    <script src="./styles/src/mdlComponentHandler.js"></script>
    <script src="./styles/src/button/button.js"></script>
    <script src="./styles/src/slider/slider.js"></script>
    <script src="./styles/src/spinner/spinner.js"></script>
    <script src="./styles/src/tooltip/tooltip.js"></script>
    <script src="./styles/src/ripple/ripple.js"></script>
    <script src="scripts/queixas.js"></script>
    <script src="scripts/pontos.js"></script>
    <!-- endbuild -->

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-XXXXX-X', 'auto');
      ga('send', 'pageview');
    </script>
  </body>
</html>