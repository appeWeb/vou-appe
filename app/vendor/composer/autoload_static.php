<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticIniteb0768a67b812361235ef37f0d1a94c8
{
    public static $prefixLengthsPsr4 = array (
        'D' => 
        array (
            'Dotenv\\' => 7,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Dotenv\\' => 
        array (
            0 => __DIR__ . '/..' . '/vlucas/phpdotenv/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticIniteb0768a67b812361235ef37f0d1a94c8::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticIniteb0768a67b812361235ef37f0d1a94c8::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
