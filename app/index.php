<?php

//set session lifetime to 24 hours
require_once('session.php');
session_start_timeout(60*60*24);

//redirect to login
if(!isset($_SESSION['user_data']['name'])){
  header('Location: login.php'); 
}
if(isset($_SESSION['route'])){
  if(isset($_GET['route']) && $_GET['route'] == "0"){
    unset($_SESSION['route']);
  }else{
    header('Location: criandorota.php');
  }
}

if(isset($_SESSION['route_follow'])){
  if(isset($_GET['route_follow']) && $_GET['route_follow'] == "0"){
    unset($_SESSION['route_follow']);
  }else{
    header('Location: seguindorota.php');
  }
}

?>
<!doctype html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--meta http-equiv="refresh" content="60; URL=https://www.vouappe.com.br"-->
    <title>Appé</title>

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">

    <!-- Web Application Manifest -->
    <link rel="manifest" href="manifest.json">

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="Appé">
    <link rel="icon" sizes="192x192" href="images/touch/chrome-touch-icon-192x192.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Appé">
    <link rel="apple-touch-icon" href="images/touch/apple-touch-icon.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#edb400">
    <!-- Material Design icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Your styles -->
    <link rel="stylesheet" href="styles/main.css">

    <!-- Jquery -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

      <!-- google maps style -->
     <script src="styles/style-maps.js"></script>
  </head>
  <body class="home-page">
    <aside class="app-lateral-menu js-lateral-menu">
      <div class="app-user">
        <img class="app-user-photo js-photo-info" alt="Sua foto" width="80" height="80">
        <h3 class="app-user-name js-user-info">Usuário</h3>
        <span class="app-user-lvl js-user-lvl"></span>
        <div class="app-user-counter">
          <span class="app-user-progress js-counter"></span>
        </div>
        <span class="app-user-points"><span class="js-pts"></span>/<strong><span class="js-limit"></span>pts</strong>
      </div>
      <div class="app-lateral-links">
        <a href="rotas.php" class="mdl-button mdl-js-button mdl-js-ripple-effect"><i class="material-icons">pin_drop</i>Suas rotas</a>
        <a href="conquistas.html" class="mdl-button mdl-js-button mdl-js-ripple-effect"><i class="material-icons">assistant_photo</i>Suas conquistas</a>
        <a href="raio.php" class="mdl-button mdl-js-button mdl-js-ripple-effect"><i class="material-icons">adjust</i>Mudar distância</a>
      </div>
    </aside>
    <header class="app-header home-header">
      <button class="app-header-menu js-hamburger mdl-button mdl-js-button mdl-button--icon">
        <i class="material-icons">menu</i>
      </button>
      <span class="app-header-title home-header-title"><img class="app-header-logo" width="70" height="46" src="/images/logo.svg" alt="Appé" /></span>
      <span class="js-app-route-indicator app-route-indicator">
        <span class="app-route-title">Na sua área:</span>
        <span class="app-route-off js-route-off"></span>
        <span class="app-route-on js-route-on"></span>
      </span>
    </header>
    <div id="google_maps" class="home-map" style="width: 100%; height: 90%; height: calc(100% - 64px);"></div>
    <footer class="home-footer">
      <a href="novarota.php" class="home-footer-btn"><i class="material-icons">pin_drop</i>Gravar rota</a>
      <a href="novaqueixa.php" class="home-footer-btn"> <i class="material-icons">announcement</i>Criar queixa</a>
    </footer>
    <!-- SNIPPETS -->
    <div id="demo-snackbar-example" class="mdl-js-snackbar mdl-snackbar">
      <div class="mdl-snackbar__text">Ative seu GPS.</div>
      <button class="mdl-snackbar__action" type="button" onclick="location.reload();">Recarregar</button>
    </div>
    <div class="js-backdrop app-backdrop"></div>
    <!-- end snippets -->
    <!-- build:js(app/) ../../scripts/main.min.js -->
    <script src="./styles/src/mdlComponentHandler.js"></script>
    <script src="./styles/src/button/button.js"></script>
    <script src="./styles/src/icon-toggle/icon-toggle.js"></script>
    <script src="./styles/src/progress/progress.js"></script>
    <script src="./styles/src/slider/slider.js"></script>
    <script src="./styles/src/spinner/spinner.js"></script>
    <script src="./styles/src/textfield/textfield.js"></script>
    <script src="./styles/src/tooltip/tooltip.js"></script>
    <script src="./styles/src/ripple/ripple.js"></script>
    <script src="scripts/pontos.js"></script>
    <script src="scripts/main.js"></script>
    <script src="scripts/home.js"></script>

   
    <!-- endbuild -->

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-XXXXX-X', 'auto');
      ga('send', 'pageview');
    </script>
    <!-- Built with love using Web Starter Kit -->

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxPQD4ZzR1urzxlcgZMHHgLfbkYFyERk8&callback=initMap"></script>
  </body>
</html>
