<?php 

session_start();

/* File for login core operations */
require_once("core.inc.php");

/* File For Database Operation */
require_once('config/db_op.class.php');

/* Constructor Invoke database class */
$database = new db_op();

/* Start database operation and login process */
if(!empty($user_profile)):

	$user_profile['oauth_type'] = $_SESSION['oauth_type'];
    if($user_profile['oauth_type'] == "facebook")
			$user_profile['picture'] = $user_profile['picture']['data']['url'];
	
    if($database->check($user_profile)>0){ //Returning User
    
        $data=$database->update($user_profile);
        $_SESSION['user_data'] = $data;                   
    } else { //New User
        
        $data=$database->insert($user_profile);
        $_SESSION['user_data'] = $data;
    }
endif;
/* End of database operation and user successfully login */

// is already logged
// redirect to index
if(isset($_SESSION['user_data']['name']) && !empty($_SESSION['user_data']))
        exit(header('Location: index.php')); 
?>
<!doctype html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vou Appé - Entrar</title>

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">

    <!-- Web Application Manifest -->
    <link rel="manifest" href="manifest.json">

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="Web Starter Kit">
    <link rel="icon" sizes="192x192" href="images/touch/chrome-touch-icon-192x192.png">
    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Web Starter Kit">
    <link rel="apple-touch-icon" href="images/touch/apple-touch-icon.png">
    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#EFCD62">
    <!-- Color the status bar on mobile devices -->
    <meta name="theme-color" content="#EFCD62">
    <!-- Your styles -->
    <link rel="stylesheet" href="styles/main.css">

    <!-- Jquery -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
  </head>

<body class="login-page">
    <div class="login-pattern"></div>
    <div class="login-container">
        <img class="login-logo" src="images/logo.svg" alt="Appé" width="150" />
        <h1 class="login-motto">Ande bem acompanhado por São Paulo.</h1>
        <?php if($loginUrl) ?>	
            <a class="login-cta" href="<?= $loginUrl ?>"><img src="images/fb_login.svg" alt="Entrar com Facebook"></a>
        <?php if($authUrl) ?> 
            <a class="login-cta" href="<?= $authUrl ?>"><img src="images/glogin.svg" alt="Entrar com Google"></a>
    </div>

<!-- build:js(app/) ../../scripts/main.min.js -->
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-XXXXX-X', 'auto');
    ga('send', 'pageview');
</script>
</body>

