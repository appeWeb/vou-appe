<?php
session_start();
//redirect to login
if(!isset($_SESSION['user_data']['name']))
  exit(header('Location: login.php')); 
  
if(!isset($_POST['route_follow']) && !isset($_SESSION['route_follow']))
  exit(header('Location: index.php'));

if(isset($_POST['route_follow']))
  $_SESSION['route_follow'] = $_POST['route_follow'];

?>

<!doctype html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Appé</title>

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">

    <!-- Web Application Manifest -->
    <link rel="manifest" href="manifest.json">

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="Appé">
    <link rel="icon" sizes="192x192" href="images/touch/chrome-touch-icon-192x192.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Appé">
    <link rel="apple-touch-icon" href="images/touch/apple-touch-icon.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#EFCD62">

    <!-- Color the status bar on mobile devices -->
    <meta name="theme-color" content="#EFCD62">

    <!-- Material Design icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Your styles -->
    <link rel="stylesheet" href="styles/main.css">
    <!-- google maps style -->
    <script src="styles/style-maps.js"></script>

    <!-- Jquery -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
  </head>
  <body class="home-page">
    <header class="app-header">
      <a href="index.php?route_follow=0" class="app-header-menu js-lateral-menu mdl-button mdl-js-button mdl-button--icon">
        <i class="material-icons">clear</i>
      </a>
      <span class="app-header-title">Seguindo Rota</span>
    </header>
    <div id="google_maps" class="home-map" style="width: 100%; height: 90%; height: calc(100% - 64px);"></div>
   
    <input type="hidden" id="route_id" value="<?=$_SESSION['route_follow']; ?>"></span>
    
    <footer class="home-footer">
      <a href="novaqueixa.php" class="home-footer-btn"> <i class="material-icons">announcement</i>Criar queixa</a>
      <a id="finish" href="index.php?route_follow=0" class="home-footer-btn close-highlight"><i class="material-icons">clear</i>Sair</a>        
    </footer>

        
    <script src="scripts/seguindorotas.js"></script>
    <script src="scripts/pontos.js"></script>
    <!-- script file to insert new route -->

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-XXXXX-X', 'auto');
      ga('send', 'pageview');
    </script>
    <!-- Built with love using Web Starter Kit -->

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxPQD4ZzR1urzxlcgZMHHgLfbkYFyERk8&callback=initMap"></script>
  </body>
</html>
