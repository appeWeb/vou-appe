<?php

session_start();
//redirect to login
if(!isset($_SESSION['user_data']['name']))
  exit(header('Location: login.php')); 

// for bd acess
require_once('config/db_op.class.php');

$database = new db_op();
$user_id = $_SESSION['user_data']['id'];
$result = $database->inserControl($user_id, 1, 0, $_POST['id'], $_POST['value']);

if($result == -1) echo(-1);
echo json_encode($result);    

?>