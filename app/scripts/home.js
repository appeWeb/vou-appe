$(document).ready(function(){
    var body = $("body");
    var hamburgerMenu = $(".js-hamburger");
    var backdrop = $(".js-backdrop");
    var lateralMenu = $(".js-lateral-menu");

    hamburgerMenu.on('click', showLateralMenu);
    $(".js-backdrop").on('click', resumeLateralMenu);

    function showLateralMenu() {
        lateralMenu.addClass("is-active");
        body.addClass("is-modal");
    }
    
    function resumeLateralMenu(e) {
        if (!lateralMenu.is(e.target)) {
            lateralMenu.removeClass("is-active");
            body.removeClass("is-modal");
        }
    }
});
