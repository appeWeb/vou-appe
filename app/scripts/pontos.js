$(window).load(function() {
    var user_id = getUrlParameter("user_id");
    getUserInfo(user_id );
    $(window).on('storage', populateUserInfo);
});

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

function getUserInfo(id = 0){
    $.post("config/get_exp.php",
    {
        user_id: id
    },
    function(data, status, response){
        if(status == "success" && data != null){
            localStorage.setItem('pontos',data.pontos);
            localStorage.setItem('ant_pontos',data.ant_pontos);
            localStorage.setItem('name',data.name);
            localStorage.setItem('picture',data.picture);
            localStorage.setItem('qnt_rotas', data.qnt_rotas);
            localStorage.setItem('qnt_grupos',data.qnt_grupos);
            localStorage.setItem('qnt_queixas',data.qnt_queixas);
            localStorage.setItem('distancia',data.distancia);

            populateUserInfo();
        }

    },"json");
}

function populateUserInfo(obj){
    var setPhoto = $(".js-photo-info");
    var setName = $(".js-user-info");
    var setUserLevel = $(".js-user-lvl");
    var pictureData = localStorage.getItem('picture');
    var nameData = localStorage.getItem('name');
    var pointsData = localStorage.getItem('pontos');

    setPhoto.attr("src", pictureData);
    setName.text(nameData);
    defineUserLevel(pointsData);
    setUserLevel.text(localStorage.getItem("lvl"));
    pointsCounter(pointsData);
}

function defineUserLevel(pts) {
    var ANDARILHO_LIMIT = 49;
    var MOCHILEIRO_LIMIT = 199;
    var PEREGRINO_LIMIT = 499;
    var GUERREIRO_LIMIT = 999;
    if (pts !== undefined && pts >= ANDARILHO_LIMIT && pts < MOCHILEIRO_LIMIT) {
        localStorage.setItem("lvl","NV2 - Mochileiro");
        localStorage.setItem("lvl_limit", MOCHILEIRO_LIMIT);
    }
    else if (pts !== undefined && pts >= MOCHILEIRO_LIMIT && pts < PEREGRINO_LIMIT) {
        localStorage.setItem("lvl","NV3 - Peregrino");
        localStorage.setItem("lvl_limit", PEREGRINO_LIMIT);
    }
    else if (pts !== undefined && pts >= PEREGRINO_LIMIT && pts < GUERREIRO_LIMIT) {
        localStorage.setItem("lvl","NV4 - Guerreiro do Asfalto");
        localStorage.setItem("lvl_limit", GUERREIRO_LIMIT);
    }
    else if (pts !== undefined && pts >= GUERREIRO_LIMIT) {
        localStorage.setItem("lvl","NV5 - Dono da Rua");
    }
    else {
        localStorage.setItem("lvl","NV1 - Andarilho");
        localStorage.setItem("lvl_limit", ANDARILHO_LIMIT);
    }
}

function pointsCounter(pts){
    var counter = $(".js-counter");
    var lvlText = $(".js-pts");
    var limitText = $(".js-limit");
    var limitData = localStorage.getItem('lvl_limit');
    var getLevelProgress = Math.round((pts * 100)/limitData);
    
    lvlText.text(pts);
    limitText.text(limitData);
    counter.css("left",-100 + getLevelProgress + "%");
    $(".js-km").text(localStorage.getItem('distancia'));
    $(".js-issues").text(localStorage.getItem('qnt_queixas'));
    $(".js-routeno").text(localStorage.getItem('qnt_rotas'));
}
