var map;
var user_marker;

var PTS_VALUE = 10;
var ANDARILHO_LIMIT = 49;
var MOCHILEIRO_LIMIT = 199;
var PEREGRINO_LIMIT = 499;
var GUERREIRO_LIMIT = 999;

var issues = [];
var coords_lat = [];
var coords_lon = [];

var getUrlParameter = function getUrlParameter(sParam) {

    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

function pointsFeedback(e) {
    e.preventDefault();
    var self = $(this);
    var targetUrl = $(this).attr('href');
    var limitData = parseInt(localStorage.getItem('lvl_limit'));
    var actualValue = parseInt(localStorage.getItem('pontos'));
    var newValue = actualValue + PTS_VALUE;

    populatePointsBar(newValue,limitData);
    $(".js-points-feedback").addClass("is-active");
    if (newValue >= ANDARILHO_LIMIT && actualValue < ANDARILHO_LIMIT){
        $(".js-points-feedback").addClass("is-nv2");
    }
    else if (newValue >= MOCHILEIRO_LIMIT && actualValue < MOCHILEIRO_LIMIT){
        $(".js-points-feedback").addClass("is-nv3");
    }
    $(".js-points-feedback").one("webkitAnimationEnd oanimationend msAnimationEnd animationend", function(){
        window.location.assign(targetUrl);
    })
}

function populatePointsBar(pts,limit) {
    var counter = $(".js-counter");
    var lvlText = $(".js-pts");
    var limitText = $(".js-limit");
    var getLevelProgress = Math.round((pts * 100)/limit);

    lvlText.text(pts);
    limitText.text(limit);
    counter.css("left",-100 + getLevelProgress + "%");
}

function map_update(latitude, longitude){

  var position = new google.maps.LatLng(latitude,longitude);
  if(!map){
    map = new google.maps.Map(document.getElementById('google_maps'), {
      zoom: 17,
      center: position,
      gestureHandling: "greedy",
      disableDefaultUI: true,
      mapTypeControl: false,
      scaleControl: false,
      styles: styles 
    });

    var icon = {
        url: "images/icons/pin-usuario.svg",
        scaledSize: new google.maps.Size(42, 42)
    };

    user_marker = new google.maps.Marker({
      position: position, 
      map: map, 
      icon: icon,
      zIndex: 2
    });
    user_marker.setMap(map);

  }else{
    map.setCenter(position);
    if(user_marker)
      user_marker.setPosition(position);
  }
}

function insertCoords(latitude,longitude){

  var last_lat;
  var last_lon;
  var istance = 0;
  if(coords_lat.length > 0){
    last_lat = coords_lat[coords_lat.length-1];
    last_lon = coords_lon[coords_lon.length-1];
    if(last_lat == latitude && last_lon == longitude)
      return;

    var pos1 = new google.maps.LatLng(last_lat,last_lon);
    var pos2 = new google.maps.LatLng(latitude,longitude);
    var distance = getDistance(pos1,pos2);
  } 

  coords_lat.push(latitude);
  coords_lon.push(longitude);

  localStorage.setItem( 'lat', JSON.stringify(coords_lat) );
  localStorage.setItem( 'lon', JSON.stringify(coords_lon) );

  $.post('config/send_coord.php',
  {
      latitude: latitude,
      longitude: longitude,
      distance: distance
  },
  function(data, status, response){
    if(response == false)
      alert('Ops, ocorreu um problema, tente novamente mais tarde.');
  });
}

function addPoint(latitude, longitude){
  var pos = new google.maps.LatLng(latitude,longitude);
  var path = rota.getPath();
  path.push(pos);
  rota.setMap(map);

  insertCoords(latitude,longitude);
}

function geo_success(position) {
  var latitude = position.coords.latitude;
  var longitude = position.coords.longitude;
  
  map_update(latitude,longitude);
  addPoint(latitude,longitude);
  getaddress(latitude,longitude);
  get_issues(latitude,longitude);  
}

function geo_error() {
  alert("Desculpe, nenhuma posição está disponível.");
}

var geo_options = {
  enableHighAccuracy: true, 
  maximumAge        : 30000, 
  timeout           : 27000
};

function initMap(){
  if(localStorage.getItem('lat') && localStorage.getItem('lon')){
    coords_lat = JSON.parse(localStorage.getItem('lat'));
    coords_lon = JSON.parse(localStorage.getItem('lon'));
  }

  var wpid = navigator.geolocation.watchPosition(geo_success, geo_error, geo_options);
  rota = new google.maps.Polyline({
      geodesic: true,
      strokeColor: '#FF6666',
      strokeOpacity: 0.8,
      strokeWeight: 5
  });

  var path = rota.getPath();
  for (i = 0; i < coords_lat.length; i++) { 
    var pos = new google.maps.LatLng(coords_lat[i],coords_lon[i]);
    path.push(pos);
  }

  rota.setMap(map);
}

function getaddress(latitude,longitude){
    var geocoder = new google.maps.Geocoder;
    var position = new google.maps.LatLng(latitude,longitude);

    geocoder.geocode({'location': position}, function(results, status) {
        if (status === 'OK') {
            if (results[0]) {
                var num = results[0].address_components["0"].long_name;
                var rua = results[0].address_components["1"].long_name;
                var address = rua+", "+num;
                sendData(latitude,longitude,address);
                localStorage.setItem("lastAddress", address);
            } else {
                console.log('No results found');
            }
        } else {
            console.log('Geocoder failed due to: ' + status);
        }
   });
}

function sendData(latitude, longitude, address){
    $.post("config/store_session.php",
    {
        latitude: latitude,
        longitude: longitude,
        address: address
    },
    function(data, status, response){});
}

var rad = function(x) {
  return x * Math.PI / 180;
};

var getDistance = function(p1, p2) {
  var R = 6378137; // Earth’s mean radius in meter
  var dLat = rad(p2.lat() - p1.lat());
  var dLong = rad(p2.lng() - p1.lng());
  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(rad(p1.lat())) * Math.cos(rad(p2.lat())) *
    Math.sin(dLong / 2) * Math.sin(dLong / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;
  return d; // returns the distance in meter
};

function get_issues(latitude, longitude){
  var pos1 = new google.maps.LatLng(latitude,longitude); 
  $.ajax({
      url: 'config/get_issues.php',
      type: 'POST',
      data: {},
      dataType : 'json',
      success: function(data, textStatus, xhr) {
        $.each(data, function(index, element) {
            var pos2 = new google.maps.LatLng(element.latitude,element.longitude); 
            var d = getDistance(pos1,pos2);
            if(d < 1000 && $.inArray(element.idqueixas, issues) == -1)
              set_issue(element);
            // else{
            //   console.log($.inArray(element.idqueixas, issues));
            // }
        });
      },
      error: function(data, xhr, textStatus, errorThrown) {}
  });  
}

//value:1 like, :0 dislike
function likes(id, value){
  $.post("likes.php",
  {
      id: id,
      value: value
  },
  function(data, status, response){
      if(response == false){
              alert('Ops, ocorreu um problema, tente novamente mais tarde.');
          }else{
            //nothing done              
            if(data == -1) return;
            
            $('#'+id+1).html(data['likes']);
            $('#'+id+0).html(data['dislikes']);
          }
  }, "json");
}

function set_issue(issue){

  var myarr = {};
  myarr["Acessidade"] = "acessibilidade"; 
  myarr["Buraco"] = "buraco";
  myarr["Iluminação"] = "iluminacao";
  myarr["Obstáculo"] = "obstaculo";
  myarr["Segurança"] = "seguranca"; 
  myarr["Sinalização"] = "sinalizacao";
  if (!issue.tipo in myarr){
    console.log("not found "+issue.tipo);
    return;
  }

  var icon = "images/icons/queixas/"+myarr[issue.tipo]+".svg";
  var position = new google.maps.LatLng(issue.latitude,issue.longitude); 

  var issue_marker = new google.maps.Marker({
      position: position, 
      map: map, 
      icon: icon,
      zIndex: 0
  });
  issue_marker.setMap(map);
  issues.push(issue.idqueixas);


  var imagehtml = "";
  if(issue.foto){
    imagehtml = '<div class="app-issue-image">'+
                    '<img src="uploads/'+issue.foto+'" alt="">'+
                   '</div>';
  }
  
  //Aqui vai o HTML que vai aparecer na janela da queixa
  var contentString = 
            imagehtml + '<div class="app-issue-box" id="content">'+
              '<div class="app-issue-title"><strong>Problemas de '+issue.tipo+ '</strong> na '+issue.endereco +
              '</div><div class="app-issue-review">'+
                issue.comentario +
              '</div> <span class="app-issue-vote">Está queixa foi útil/válida?</span><br><i class="material-icons" onClick="likes('+issue.idqueixas+',1)"></span>&nbsp;thumb_up </i>' +
              '<span id="'+issue.idqueixas+'1">'+issue.likes+'</span>&nbsp;&nbsp;&nbsp;' + 
              //dislike icon html
              '<i class="material-icons" onClick="likes('+issue.idqueixas+',0)"></span>&nbsp;thumb_down </i>' +
              '<span id="'+issue.idqueixas+'0">'+issue.dislikes+'</span>' +
            '</div>';
            
  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });

  issue_marker.addListener('click', function() {
    infowindow.open(map, issue_marker);
  });
}

$(window).load(function() {
  $("#finish").click( function (){
      localStorage.removeItem('coords');
  });

  $(".js-points-trigger").on('click', pointsFeedback);
});



