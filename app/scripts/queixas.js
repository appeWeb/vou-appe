$(document).ready(function(){

    var CHARACTER_LIMIT = 120;
    var PTS_VALUE = 1;
    var ANDARILHO_LIMIT = 49;
    var MOCHILEIRO_LIMIT = 199;
    var PEREGRINO_LIMIT = 499;
    var GUERREIRO_LIMIT = 999;

    $(".js-issue-step1 input").on("change", checkCategorySelection);
    $(".js-img-upload").on("change", function() {
        displayUploadedImage(this);
    });
    $("#next-step-btn").on("click", nextStep);
    $(".js-required-field").on("keyup", checkRequiredFields);
    $(".js-point-trigger").on("submit", pointsFeedback);

    function checkCategorySelection(e) {
       if($(e.target).is(':checked')){  
           var issueCategory = e.target.id;
           var issueCategoryName = e.target.value;
           $("#next-step-btn").removeAttr("disabled");
           localStorage.setItem("lastIssueCategory", issueCategory);   
           localStorage.setItem("lastIssueCategoryName", issueCategoryName);         
       }
    }

    function nextStep() {
        var getAddress = localStorage.getItem("lastAddress");
        var getIssueCategory = localStorage.getItem("lastIssueCategory");
        var getIssueCategoryName = localStorage.getItem("lastIssueCategoryName");
        $("body").addClass("is-step2");
        $(".js-address").text(getAddress);
        $(".js-category-id").attr("src","images/icons/queixas/" + getIssueCategory + ".svg");
        $(".js-category-name").text(getIssueCategoryName);
    }

    function displayUploadedImage(input) {
        var label = $(".js-img-upload-btn");
       if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.js-uploaded').attr('src', e.target.result);
                label.find("i").text("done");
                label.addClass("uploaded");
            };
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    function checkRequiredFields(e) {
        var fieldLength = $(e.target).val().length;
        var remainingChar = CHARACTER_LIMIT - fieldLength;
        var limitString = remainingChar + "/" + CHARACTER_LIMIT;
        var submitCta = $("#submit-btn");

        $(".js-issue-textarea").attr("data-limit", limitString);
        if(fieldLength && fieldLength > 5) {
           submitCta.removeAttr("disabled");
        }
        else {
            submitCta.attr("disabled",true);
        }      
    }

    function pointsFeedback(e) {
        e.preventDefault();
        var self = $(this);
        var limitData = parseInt(localStorage.getItem('lvl_limit'));
        var actualValue = parseInt(localStorage.getItem('pontos'));
        var newValue = actualValue + PTS_VALUE;

        populatePointsBar(newValue,limitData);
        $(".js-points-feedback").addClass("is-active");
        if (newValue >= ANDARILHO_LIMIT && actualValue < ANDARILHO_LIMIT){
            $(".js-points-feedback").addClass("is-nv2");
        }
        else if (newValue >= MOCHILEIRO_LIMIT && actualValue < MOCHILEIRO_LIMIT){
            $(".js-points-feedback").addClass("is-nv3");
        }
        $(".js-points-feedback").one("webkitAnimationEnd oanimationend msAnimationEnd animationend", function(){
            self.off('submit').submit();
        })
    }

    function populatePointsBar(pts,limit) {
        var counter = $(".js-counter");
        var lvlText = $(".js-pts");
        var limitText = $(".js-limit");
        var getLevelProgress = Math.round((pts * 100)/limit);

        lvlText.text(pts);
        limitText.text(limit);
        counter.css("left",-100 + getLevelProgress + "%");
    }
});
