var map;
var user_marker;
var issues = [];
var routes = [];
var ativas = 0;
var inativas = 0;
var radiusValue = parseInt(localStorage.getItem('radius'));
var flag = false;

(function() {
  'use strict';

  var isLocalhost = Boolean(window.location.hostname === 'localhost' || window.location.hostname === '[::1]' || window.location.hostname.match(/^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/));

  if ('serviceWorker' in navigator && (window.location.protocol === 'https:' || isLocalhost)) {
    navigator.serviceWorker.register('service-worker.js')
    .then(function(registration) {
      registration.onupdatefound = function() {
        if (navigator.serviceWorker.controller) {
          var installingWorker = registration.installing;

          installingWorker.onstatechange = function() {
            switch (installingWorker.state) {
              case 'installed':
                break;

              case 'redundant':
                throw new Error('The installing ' +
                                'service worker became redundant.');

              default:
            }
          };
        }
      };
    }).catch(function(e) {
      console.log('Error during service worker registration:', e);
    });
  }
})();


(function(a){
    a.fn.rating_widget = function(p){
        var p = p||{};
        var b = p&&p.starLength?p.starLength:"5";
        var c = p&&p.callbackFunctionName?p.callbackFunctionName:"";
        var e = p&&p.initialValue?p.initialValue:"0";
        var d = p&&p.imageDirectory?p.imageDirectory:"images";
        var r = p&&p.inputAttr?p.inputAttr:"";
        var f = e;
        var g = a(this);
        b = parseInt(b);
        init();
        g.next("ul").children("li").hover(function(){
            $(this).parent().children("li").css('background-position','0px 0px');
            var a = $(this).parent().children("li").index($(this));
            $(this).parent().children("li").slice(0,a+1).css('background-position','0px -28px')
        },function(){});
        g.next("ul").children("li").click(function(){
            var a = $(this).parent().children("li").index($(this));
            var attrVal = (r != '')?g.attr(r):'';
            f = a+1;
            g.val(f);
            if(c != ""){
                eval(c+"("+g.val()+", "+attrVal+")")
            }
        });
        g.next("ul").hover(function(){},function(){
            if(f == ""){
                $(this).children("li").slice(0,f).css('background-position','0px 0px')
            }else{
                $(this).children("li").css('background-position','0px 0px');
                $(this).children("li").slice(0,f).css('background-position','0px -28px')
            }
        });
        function init(){
            if(!radiusValue) {
                window.location.replace("/raio.php");
            }

            $('<div style="clear:both;"></div>').insertAfter(g);
            g.css("float","left");
            var a = $("<ul>");
            a.addClass("rating_widget");
            for(var i=1;i<=b;i++){
                a.append('<li style="background-image:url('+d+'widget_star.svg)"><span>'+i+'</span></li>')
            }
            a.insertAfter(g);
            if(e != ""){
                f = e;
                g.val(e);
                g.next("ul").children("li").slice(0,f).css('background-position','0px -28px')
            }
        }
    }
})(jQuery);

$(window).load(function() {
    var status = getUrlParameter("rota_status");
    var user_id = getUrlParameter("user_id");
    if(status == "1"){
        alert("Parabéns! você criou a rota com sucesso");
        localStorage.removeItem('lat');
        localStorage.removeItem('lon');
    }
    else if(status == 0){
        alert("Ops, houve uma falha ao criar a rota, tente novamente mais tarde");
    }

    $("#rating_star").rating_widget({
        starLength: '5',
        initialValue: '',
        callbackFunctionName: 'processRating',
        imageDirectory: 'images/',
        inputAttr: 'route_id'
    });

    $("#finish").click( function (){
        localStorage.removeItem('coords');
    });
});

$( window ).ajaxStop(function() {  
   if (flag == false && (ativas != 0 || inativas != 0)){
        flag = true;
        var str1, str2;
        if(ativas == 0)
            str1 = "0 rota ativa";
        else if (ativas == 1)
            str1 = "1 rota ativa";
        else 
            str1 = ativas+" rotas ativas";

        if(inativas == 0)
            str2 = "0 rota inativa";
        else if (inativas == 1)
            str2 = "1 rota inativa";
        else 
            str2 = inativas+" rotas inativas";

        $(".js-route-on").text(str1);
        $(".js-route-off").text(str2);
        $(".js-app-route-indicator").addClass("is-active");

        ativas = inativas = 0;
    }
    
});

function map_update(latitude, longitude){  
  var position = new google.maps.LatLng(latitude,longitude);
  if(!map){    
    map = new google.maps.Map(document.getElementById('google_maps'), {
      zoom: 16,
      center: position,
      gestureHandling: "greedy",
      disableDefaultUI: true , 
      styles: styles       //  comment for normal map
    });

    var icon = {
        url: "images/icons/pin-usuario.svg",
        scaledSize: new google.maps.Size(50, 50)
    };

    user_marker = new google.maps.Marker({
      position: position, 
      map: map, 
      icon: icon,
      zIndex: 0
    });
    user_marker.setMap(map);

    // Add circle overlay and bind to marker
    var circle = new google.maps.Circle({
      map: map,
      radius: radiusValue,         // in meters
      fillColor: '#FFFFFF',
      strokeColor: '#000000',
      strokeOpacity: 0.2,
      strokeWeight: 2
    });
    circle.bindTo('center', user_marker, 'position');
  }else{
    map.setCenter(position);
    if(user_marker)
      user_marker.setPosition(position);
  }
}

function geo_success(position) {
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;

    map_update(latitude,longitude);
    getaddress(latitude,longitude);

    // get_issues(latitude,longitude);
    get_routes(latitude,longitude);
}

function geo_error() {
    $(".mdl-js-snackbar").addClass("show-snack");
    $("#google_maps").css("opacity",".7");
}

var geo_options = {
  enableHighAccuracy: true
};

function initMap(){
  var wpid = navigator.geolocation.watchPosition(geo_success, geo_error, geo_options);
}

function addPontoRota(latitude, longitude){
  var path = poly.getPath();
  path.push(event.latLng);
}

function getaddress(latitude,longitude){
    var geocoder = new google.maps.Geocoder;
    var position = new google.maps.LatLng(latitude,longitude);

    geocoder.geocode({'location': position}, function(results, status) {
        if (status === 'OK') {
            if (results[0]) {
                var num = results[0].address_components["0"].long_name;
                var rua = results[0].address_components["1"].long_name;
                var address = rua+", "+num;
                sendData(latitude,longitude,address);
                localStorage.setItem("lastAddress", address);
            }   else {
                 console.log('Can not get the address!');
            }
        } else {
            console.log('Geocoder failed due to: ' + status);
        }
   });
}

function sendData(latitude, longitude, address){
    if(address){
        var newaddress = address.split(',');
        newaddress = newaddress[0]+", "+newaddress[1];
    }

    $.post("config/store_session.php",
    {
        latitude: latitude,
        longitude: longitude,
        address: address
    });
}

var rad = function(x) {
    return x * Math.PI / 180;   
};

var getDistance = function(p1, p2) {
    var R = 6378137; // Earth’s mean radius in meter
    var dLat = rad(p2.lat() - p1.lat());
    var dLong = rad(p2.lng() - p1.lng());
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(rad(p1.lat())) * Math.cos(rad(p2.lat())) *
        Math.sin(dLong / 2) * Math.sin(dLong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d; // returns the distance in meter
};

function get_routes(latitude, longitude){
    var pos1 = new google.maps.LatLng(latitude,longitude); 
    // console.log("**MODULO DE PEGAR E INSERIR NO MAPS AS ROTAS**");
    $.ajax({
        url: 'config/get_routes.php',
        type: 'POST',
        data: {},
        dataType : 'json',
        success: function(data, textStatus, xhr) {
            $.each(data, function(index, route) {
                //use id = route.rota_id; 
                var pos2 = new google.maps.LatLng(route.latitude,route.longitude); 
                var d = getDistance(pos1,pos2);
                if(d < radiusValue){
                    set_routes(route,ativas,inativas);
                }
            });      
        },
        error: function(data, xhr, textStatus, errorThrown) {
            console.log("erro : "+JSON.stringify(textStatus));
            console.log("data : "+JSON.stringify(data));
        }
    });    
}



function set_routes(route){
    var id = route.rota_id;    
    $.post("config/check_route.php",
    {
        route_id: id
    },
    function(data, status, response){
        if(response == false){
                alert('Ops, ocorreu um problema, tente novamente mais tarde.');
            }else{
                if(data == -1) 
                     console.log("ocorreu um erro ao ler as rotas ativas!");
                else{

                    if(data > 0){
                        var icon = "images/icons/pin-rota.svg";                        
                        ativas += 1;
                    }else{
                        var icon = "images/icons/pin-rota-inativa.svg";
                        inativas += 1;
                    }
                    var position = new google.maps.LatLng(route.latitude,route.longitude); 

                    var route_marker = new google.maps.Marker({
                        position: position, 
                        map: map, 
                        icon: icon
                    });
                    route_marker.setMap(map);
                    routes.push(route.rota_id);

                    route_marker.addListener('click', function() {
                        window.location.replace("rotastatus.php?route_id="+route.rota_id);
                        return false;
                    });
                }    
            }
    });
}

//value:1 like, :0 dislike
function likes(id, value){
  $.post("likes.php",
  {
      id: id,
      value: value
  },
  function(data, status, response){
      if(response == false){
              alert('Ops, ocorreu um problema, tente novamente mais tarde.');
          }else{
            //nothing done              
            if(data == -1) return;
            
            $('#'+id+1).html(data['likes']);
            $('#'+id+0).html(data['dislikes']);
          }
  }, "json");
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

function processRating(nota, route_id){
    var route_id = getUrlParameter("route_id");
    $.ajax({
        type: 'POST',
        url: 'config/rating.php',
        data: 'route_id='+route_id+'&nota='+nota,
        dataType: 'json',
        success : function(data) {
            if (data.status == 'ok') {
                $('#avgrat').text(Math.round(data.nota/data.qnt_votos * 100) / 100 );
                $('#totalrat').text(data.qnt_votos);
                alert('Obrigado por votar!');
            }else{
                console.log("data = "+data);
                console.log("data nota= "+data.nota);
                alert('Ocorreu um problema ao votar, por favor tente mais tarde!');
            }
        }
    });
}

//0 = request
//1 = approval
function approval(user_id, route_id, type){
    $.ajax({
        type: 'POST',
        url: 'config/groups.php',
        data: 'route_id='+route_id+'&user_id='+user_id+'&type='+type,
        dataType: 'json',
        success : function(data) {
            if(type == 0)
                alert('Requisiçao feita! Por favor aguarde a aprovaçao do administrador.');
            else if(type == 1)
                alert('Aprovado com sucesso!');
                
            location.reload();
        }
    });
}

function sendMessage(user_id, route_id){

    var message = $("#message").val();
    if(!message) return;
    $.post("config/send_message.php",
    {
        user_id: user_id,
        route_id: route_id,
        message: message
    },
    function(data, status, response){
        location.reload();
    });
}