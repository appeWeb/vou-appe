$(".js-prev").on('click', prevStep);
$(".js-next").on('click', nextStep);
$(".js-set-radius").on('click', setRadiusValue);

function prevStep(e){
    var btn = $(e.target);
    var selectedItem = btn.parent();
    selectedItem.removeClass("is-selected");
    selectedItem.prev().addClass("is-selected");
}

function nextStep(e){
    var btn = $(e.target);
    var selectedItem = btn.parent();
    selectedItem.removeClass("is-selected");
    selectedItem.next().addClass("is-selected");
}

function setRadiusValue(){
    var radiusValue = $(".is-selected").data('value');
    var radiusInt = parseInt(radiusValue);
    
    localStorage.setItem('radius', radiusInt);
    window.location.replace("/index.php");
}