<?php
session_start();
//redirect to login
if(!isset($_SESSION['user_data']['name']))
  exit(header('Location: login.php')); 
ini_set('default_charset','UTF-8');

?>

<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Appé - Criar queixa</title>

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">

    <!-- Web Application Manifest -->
    <link rel="manifest" href="manifest.json">

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="Appé">
    <link rel="icon" sizes="192x192" href="images/touch/chrome-touch-icon-192x192.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Appé">
    <link rel="apple-touch-icon" href="images/touch/apple-touch-icon.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#edb400">

    <!-- Material Design icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Your styles -->
    <link rel="stylesheet" href="styles/main.css">

    <!-- Jquery -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
  </head>
   <body class="issue-page is-footer-fixed">
    <header class="app-header">
      <a href="/" class="app-header-menu js-lateral-menu mdl-button mdl-js-button mdl-button--icon">
        <i class="material-icons js-back">arrow_back</i>
      </a>
      <span class="app-header-title">Nova queixa</span>
    </header>
    <section class="issue-info-container">
      <div class="js-issue-about issue-about app-intro">
        <img class="js-category-id app-intro-illustration" src="images/icons/queixas/acessibilidade.svg" />
        <div class="issue-about-content app-intro-content">
          <span class="js-category-name issue-about-title app-intro-title">Queixa</span>
          <span class="issue-about-subtitle app-intro-subtitle">na <span class="js-address">Cidade de São Paulo</span></span>
        </div>
      </div>
      <div class="js-issue-explain issue-explain app-intro">
        <img class="app-intro-illustration" src="images/illustrations/issue_graph.svg" />
        <div class="issue-explain-content app-intro-content">
          <span class="issue-explain-title app-intro-title">Teve problemas com o trajeto?</span>
          <span class="issue-explain-subtitle app-intro-subtitle">Então ajude-nos a identificar os problemas da nossa cidade.</span>
        </div>
      </div>
    </section>
    <section class="issue-body content-body">

      <form class="js-point-trigger issue-form" enctype="multipart/form-data" action="config/send_issue.php" accept-charset="utf-8" method="post">
        <fieldset class="js-issue-step1 issue-step for-step1">
          <h2>Qual o tipo de problema?</h2>
          <input type="radio" id="buraco" name="issue" value="Buraco">
          <label class="issue-select" for="buraco">
            <span class="issue-select-icon"></span>Buracos
          </label>

          <input type="radio" id="acessibilidade" name="issue" value="Acessibilidade">
          <label class="issue-select" for="acessibilidade">
            <span class="issue-select-icon"></span>Acessibilidade
          </label>
          
          <input type="radio" id="iluminacao" name="issue" value="Iluminação">
          <label class="issue-select" for="iluminacao">
            <span class="issue-select-icon"></span>Iluminação
          </label>

          <input type="radio" id="obstaculo" name="issue" value="Obstáculo">
          <label class="issue-select" for="obstaculo">
            <span class="issue-select-icon"></span>Obstáculo
          </label>

          <input type="radio" id="seguranca" name="issue" value="Segurança">
          <label class="issue-select" for="seguranca">
            <span class="issue-select-icon"></span>Segurança
          </label>

          <input type="radio" id="sinalizacao" name="issue" value="Sinalização">
          <label class="issue-select" for="sinalizacao">
            <span class="issue-select-icon"></span>Sinalização
          </label>
        </fieldset>

        <fieldset class="js-issue-step2 issue-step for-step2">
          <h2>Fale sobre o problema</h2> 
          <div class="issue-textarea js-issue-textarea" data-limit="120/120"><textarea required maxlength="120" class="js-required-field" rows="2" name="problem" value="" placeholder="Pode soltar o verbo! Escreva brevemente um resumo do problema."></textarea></div>
          <h2>Tem como você tirar uma foto?</h2>
          <span class="subtitle">Seria fantástico, mas não é obrigatório.</span>
          <input class="js-img-upload" type="file" accept="image/*;capture=camera" name="image" id="image"></input>
          <label class="issue-select-img js-img-upload-btn" for="image">
            <img src="#" class="js-uploaded" />
            <i class="material-icons">add_a_photo</i>
          </label>
        </fieldset>
        <footer class="issue-footer">
          <span disabled id="next-step-btn" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary">Prosseguir&nbsp;<i class="material-icons">keyboard_arrow_right</i></span>
          <button disabled id="submit-btn" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary">Enviar</button>
        </footer>
      </form>        
    </section>

     <div class="js-points-feedback app-points-feedback app-user">
      <div class="nv2">
        <div class="app-points-level">
          <img src="images/illustrations/mochileiro.png" width="120px" height="120" />
        </div>
        <h3 class="app-points-title app-got-level">Você subiu de nível!</h3>
        <div class="app-points-levelname">NV2 - Mochileiro</div>
      </div>
      <div class="nv3">
        <div class="app-points-level">
          <img src="images/illustrations/peregrino.png" width="120px" height="120" />
        </div>
        <h3 class="app-points-title app-got-level">Você subiu de nível!</h3>
        <div class="app-points-levelname">NV3 - Peregrino</div>
      </div>
      <h3 class="app-points-title app-got-prize">Você ganhou pontos!</h3>
      <div class="app-user-counter">
        <span class="app-user-progress js-counter"></span>
      </div>
      <span class="app-user-points"><span class="js-pts"></span>/<strong><span class="js-limit"></span>pts</strong>
      <div class="login-pattern"></div>
    </div>
   <!-- build:js(app/) ../../scripts/main.min.js -->
    <script src="./styles/src/mdlComponentHandler.js"></script>
    <script src="./styles/src/button/button.js"></script>
    <script src="./styles/src/slider/slider.js"></script>
    <script src="./styles/src/spinner/spinner.js"></script>
    <script src="./styles/src/tooltip/tooltip.js"></script>
    <script src="./styles/src/ripple/ripple.js"></script>
    <script src="scripts/queixas.js"></script>
    <script src="scripts/pontos.js"></script>
    <!-- endbuild -->

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-XXXXX-X', 'auto');
      ga('send', 'pageview');
    </script>
  </body>
</html>
