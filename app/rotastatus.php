<?php

session_start();
//redirect to login
if(!isset($_SESSION['user_data']['name']))
 exit( header('Location: login.php')); 
if(!isset($_GET['route_id']))
 exit(header('Location: index.php'));

// for bd acess
ini_set('default_charset','UTF-8');
require_once('config/db_op.class.php');

$user_id = $_SESSION['user_data']['id'];
$route_id = $_GET['route_id'];

$database = new db_op();
$rota = $database->selectRoute($route_id);
$nota = $database->selectRouterating($route_id);
$mensagens = $database->getMessages($route_id);

$inside = $approved = $dono = false;
$group = $database->getGroup($route_id);
foreach($group as $user){
  if($user['usuario_id'] == $user_id){
    //user is in group
    $inside = true;
    if($user['aprovado'] == 1){
      $approved = true;
    }
  }
}

if($rota['usuario_id'] == $user_id)
  $dono = true;

$notatotal = $nota['nota'];
$qnt_votos = $nota['qnt_votos'];
$avgnota = 0;
if($qnt_votos >= 1){
  $avgnota = round($notatotal/$qnt_votos,2);
}
?>

<!doctype html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Appé</title>

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">

    <!-- Web Application Manifest -->
    <link rel="manifest" href="manifest.json">

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="Appé">
    <link rel="icon" sizes="192x192" href="images/touch/chrome-touch-icon-192x192.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Appé">
    <link rel="apple-touch-icon" href="images/touch/apple-touch-icon.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#EFCD62">

    <!-- Material Design icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Your styles -->
    <link rel="stylesheet" href="styles/main.css">

    <!-- Jquery -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
  </head>
   <body class="route-status">
    <header class="app-header">
      <a href="/" class="app-header-menu js-lateral-menu mdl-button mdl-js-button mdl-button--icon">
        <i class="material-icons">arrow_back</i>
      </a>
      <span class="app-header-title route-status-title"><?=$rota['inicio']?> - <?=$rota['fim']?></span>
      <span class="subtitle">Rota de <?= ceil($rota['tempo']/60) ?> min</span>
    </header>
    <div class="route-status-map">
    </div>
    <div class="route-status-container">
      <div class="route-status-card mdl-card mdl-shadow--2dp">
        <?php if($inside === false){ ?>
          <button class="request-btn mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" type="text" onclick="approval(<?=$user_id.','.$route_id; ?>,0);" id="request">
            <i class="material-icons">no_encryption</i> Participar
          </button>
          <p class="request-disclaimer">Peça permissão pra entrar nessa rota.</p>
        <?php }else if($inside === true && $approved === false){ ?>
          <div class="request-pending"><div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div> Esperando aprovação do criador</div>
        <?php }else if($inside === true && $approved === true){ ?>
          <form action="seguindorota.php" method="post">
              <input hidden id="route_follow" name="route_follow" value="<?=$route_id;?>">
              <button class="follow-btn mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--primary" type="submit" value="Seguir Rota">
                <i class="material-icons">play_arrow</i>
              </button>
            </form>
        <?php } ?>
        <h2 class="route-status-card__title">Sobre a Rota</h2>
        <div class="route-status-type route-status-itens">
          <div class="route-status-type__icons">
            <span class="route-status-overall">Nota <?=$avgnota; ?> <small><?=$qnt_votos; ?> <?=($qnt_votos = 1) ? "avaliação" : "avaliações";?></small></span>
          </div>
          <input name="rating" value="0" id="rating_star" type="hidden" route_id="1"/>
        </div>

        <div class="route-status-type route-status-itens">
          <div class="route-status-type__icons">
            <div class="route-status-type__icon <?=$rota['categoria1']?>"></div>
            <div class="route-status-type__icon <?=$rota['categoria2']?>"></div>
          </div>
          <h3 class="route-status-type__title">Essa rota é <strong><?=$rota['categoria1']?></strong> e <strong><?=$rota['categoria2']?></strong></h3>
        </div>

        <div class="route-status-type route-status-itens">
          <div class="route-status-type__icons">
            <img class="route-status-photo" src="<?=$rota['picture'] ?>" width="60" height="60" />
          </div>
          <h3 class="route-status-type__title">Criada por <strong><?=$rota['name']; ?></strong></h3>
        </div>
      </div>
      <?php if($inside === true && $approved === true){ ?>
        <div class="route-status-card mdl-card mdl-shadow--2dp">
          <h2 class="route-status-card__title">Chat da Rota</h2>
          <div class="mdl-card__supporting-text route-status-messages">
            <?php foreach($mensagens as $mensagem){ 
                  echo "<div class='route-status-message'>";
                    echo "<span>".$mensagem['message'];
                    echo "<sub>".$mensagem['name']." às ".$mensagem['created']."</sub></span>";
                  echo "</div>";
              } ?>
          </div>
          <div class="mdl-card__actions mdl-card--border route-status-action">
            <textarea maxlength="140" class="js-required-field" rows="1" id="message" value="" placeholder="Dê um oi pro pessoal!"></textarea>
            <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary" type="text" onclick='sendMessage(<?=$user_id.",".$route_id; ?>)'>Enviar</button>
          </div>
      </div>
    <?php } ?>
    <?php if($inside === true && $approved === true){ ?>
    <div class="route-status-card mdl-card mdl-shadow--2dp">
          <h2 class="route-status-card__title">Quem está no grupo:</h2>
          <div class="mdl-card__supporting-text">
            <?php foreach($group as $user){
              if($user['aprovado'] == 1){
                echo "<p class='route-status-users' id='".$user['id']."'>";
                echo "<img width='45' height='45' class='user_pic route-status-photo' src='".$user['picture']."' alt='Foto do membro da rota'>";
                echo "".$user['name'];
                echo "</p>"; 
              }
            } ?>   
          </div>
      </div>
      <?php } ?>
      <?php if($dono === true && count($group) > 1){ ?>
      <div class="route-status-card mdl-card mdl-shadow--2dp">
          <h2 class="route-status-card__title">Pedidos Pendentes:</h2>
          <div class="mdl-card__supporting-text">
            <?php foreach($group as $user){
              if($user['aprovado'] == 0){
                echo "<p class='route-status-users pending' id='".$user['id']."'>";
                echo "<img width='45' height='45' class='user_pic route-status-photo' src='".$user['picture']."' alt='Membro Pendente'>";
                echo "".$user['name'];
                echo "<button class='mdl-button mdl-js-button mdl-button--icon' type='text' onclick='approval(".$user['usuario_id'].",".$route_id.",1)'><i class='material-icons'>done</i></button>";
                echo "</p>"; 
              }
            } ?>   
          </div>
      </div>
      <?php } ?>
</div>

   <!-- build:js(app/) ../../scripts/main.min.js -->
    <script src="./styles/src/mdlComponentHandler.js"></script>
    <script src="./styles/src/button/button.js"></script>
    <script src="./styles/src/slider/slider.js"></script>
    <script src="./styles/src/spinner/spinner.js"></script>
    <script src="./styles/src/tooltip/tooltip.js"></script>
    <script src="./styles/src/ripple/ripple.js"></script>
    <script src="scripts/main.js"></script>
    <script src="scripts/pontos.js"></script>
    <!-- endbuild -->

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-XXXXX-X', 'auto');
      ga('send', 'pageview');
    </script>
  </body>
</html>
