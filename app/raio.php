<?php

session_start();
//redirect to login
if(!isset($_SESSION['user_data']['name']))
  header('Location: login.php'); 

// for bd acess
ini_set('default_charset','UTF-8');
require_once('config/db_op.class.php');

$user = $_SESSION['user_data'];
$id =  $user['id'];
if(isset($_GET['user_id']))
  $id = $_GET['user_id'];
?>

<!doctype html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Selecione sua distância - Appé</title>

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">

    <!-- Web Application Manifest -->
    <link rel="manifest" href="manifest.json">

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="Appé">
    <link rel="icon" sizes="192x192" href="images/touch/chrome-touch-icon-192x192.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Appé">
    <link rel="apple-touch-icon" href="images/touch/apple-touch-icon.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#EFCD62">

    <!-- Material Design icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Your styles -->
    <link rel="stylesheet" href="styles/main.css">

    <!-- Jquery -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    </head>
    <body class="radius-page">
        <header class="radius-explain">
            <div class="radius-container">
                <span class="radius-explain-title">O quanto você quer andar?</span>
                <span class="radius-explain-subtitle">Defina até quantos km você está disposto a andar.<br /> Limitaremos as rotas disponíveis a essa distância.</span>
            </div>
        </header>
        <section class="radius-body">
            <div class="radius-select-container">
                <div class="radius-select-item is-selected" data-value="1000">
                        <img class="radius-select-illustration" src="images/illustrations/1km.jpg" />
                        <h4>Para quem quer se acostumar com a ideia. Vá no seu ritmo!</h4>
                        <i class="radius-select-arrow material-icons next js-next">keyboard_arrow_right</i>
                    
                </div>
                <div class="radius-select-item" data-value="2000">
                        <img class="radius-select-illustration" src="images/illustrations/2km.jpg" />
                        <h4>Perfeita para começar a andar por ai. É a nossa recomendação!</h4>
                        <i class="radius-select-arrow material-icons prev js-prev">keyboard_arrow_left</i>
                        <i class="radius-select-arrow material-icons next js-next">keyboard_arrow_right</i>
                    
                </div>
                <div class="radius-select-item" data-value="3000">
                        <img class="radius-select-illustration" src="images/illustrations/3km.jpg" />
                        <h4>Aproveite a maior diversidade de rotas. Viva sua cidade!</h4>
                        <i class="radius-select-arrow material-icons prev js-prev">keyboard_arrow_left</i>
                        <i class="radius-select-arrow material-icons next js-next">keyboard_arrow_right</i>
                    
                </div>
                <div class="radius-select-item" data-value="4000">
                        <img class="radius-select-illustration" src="images/illustrations/4km.jpg" />
                        <h4>Essa é a opção feita pra agradar quem já está acostumado a caminhar.</h4>
                        <i class="radius-select-arrow material-icons prev js-prev">keyboard_arrow_left</i>
                        <i class="radius-select-arrow material-icons next js-next">keyboard_arrow_right</i>
                    
                </div>
                <div class="radius-select-item" data-value="5000">
                        <img class="radius-select-illustration" src="images/illustrations/5km.jpg" />
                        <h4>A fronteira final: a cidade já está a seus pés.</h4>
                        <i class="radius-select-arrow material-icons prev js-prev">keyboard_arrow_left</i>
                    
                </div>
            </div>
        </section>
        <footer class="issue-footer">
          <span type="submit" class="js-set-radius mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary">Definir distância</span>
        </footer>
        <!-- build:js(app/) ../../scripts/main.min.js -->
        <script src="./styles/src/mdlComponentHandler.js"></script>
        <script src="./styles/src/button/button.js"></script>
        <script src="./styles/src/slider/slider.js"></script>
        <script src="./styles/src/spinner/spinner.js"></script>
        <script src="./styles/src/tooltip/tooltip.js"></script>
        <script src="./styles/src/ripple/ripple.js"></script>
        <script src="scripts/basic.js"></script>
        <!-- endbuild -->

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID -->
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-XXXXX-X', 'auto');
        ga('send', 'pageview');
        </script>
  </body>
</html>
