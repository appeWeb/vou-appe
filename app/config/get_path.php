<?php

session_start();

//redirect to login
if(!isset($_SESSION['user_data']['name']))
  exit(header('Location: login.php')); 

if(!isset($_POST['route_id']))
    echo "err";
else{
    // for bd acess
    require_once('db_op.class.php');
    $database = new db_op();

    $result = $database->getPath($_POST['route_id']);

    echo json_encode($result);
}
?>