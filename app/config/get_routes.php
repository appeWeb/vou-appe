<?php

session_start();
//redirect to login
if(!isset($_SESSION['user_data']['name']))
  exit(header('Location: login.php')); 


// for bd acess
require_once('db_op.class.php');

$database = new db_op();


if (isset($_POST['route_id'])){
  $result = $database->checkRouteStatus($_POST['route_id']);
  return $result;
}else{
  $result = $database->getRoutes();

  //return to main javascript
  echo json_encode($result);    
}
?>