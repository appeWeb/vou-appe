<?php

//for db search
require_once('db_op.class.php');
session_start();

if(!empty($_POST['nota'])){
    $database = new db_op();
    $rota = $database->updateRouteRating($_POST['route_id'], $_POST['nota'], $_SESSION['user_data']['id']);

    $ratingRow = $database->selectRouteRating($_POST['route_id']);
    if($ratingRow){
        $ratingRow['status'] = 'ok';
    }else{
        $ratingRow['status'] = 'err';
    }
    
    //Return json formatted rating data
    echo json_encode($ratingRow);
}
?>