<?php

session_start();

//redirect to login
if(!isset($_SESSION['user_data']['name']))
  exit(header('Location: login.php')); 

if(!isset($_SESSION['route_follow']) || !isset($_POST['type'])){
    echo "err";
}    
else{
    // for bd acess
    require_once('db_op.class.php');
    $database = new db_op();
    $result = $database->updateRouteStatus($_SESSION['route_follow'],$_POST['type'],$_SESSION['user_data']['id']);
    echo json_encode($result);
}
?>