<?php

session_start();

//redirect to login
if(!isset($_SESSION['user_data']['name']))
  exit(header('Location: login.php')); 

if(!isset($_POST['user_id']))
    echo "err";
else{
    // for bd acess
    require_once('db_op.class.php');
    $database = new db_op();
    $id = $_POST['user_id'];
    if($id == 0)
        $id = $_SESSION['user_data']['id'];

    $result = $database->getExp($id);

    echo json_encode($result);
}
?>