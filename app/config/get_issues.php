<?php

session_start();
//redirect to login
if(!isset($_SESSION['user_data']['name']))
  exit(header('Location: login.php')); 

// for bd acess
require_once('db_op.class.php');

$database = new db_op();
$result = $database->selectQueixas();

echo json_encode($result);    

?>