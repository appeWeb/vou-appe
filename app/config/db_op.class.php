<?php

class db_op{
	// CONSTRUCTOR SETTINGS
    function __construct() {

	   	global $dbh;
		date_default_timezone_set('America/Sao_Paulo');
		
		//database configuration
		require __DIR__ . '/../vendor/autoload.php';
		$dotenv = new Dotenv\Dotenv(__DIR__, 'config.env');
		$dotenv->load();
		
		$host = getenv('DB_HOST');
		$data_b = getenv('DB_TABLE');
		$db_user = getenv('DB_USER');
		$db_pwd = getenv('DB_PASS');

		try { // attempt to create a connection to database
			$dbh = new PDO("mysql:charset=utf8mb4;host=".$host.";dbname=".$data_b,$db_user,$db_pwd);
		}catch(PDOException $e) { // if it fails, we echo the error and die.
			echo $e->getMessage();
			die();
		}
	}

	/* BEGIN USER FUNCTIONS */
	public function select($email)
	{
		global $dbh;
		$select = $dbh->prepare("SELECT * FROM usuarios WHERE email='".$email."'");
		$select->execute();
		return $select->fetch();
	}

	public function selectOne($id){
		global $dbh;
		$select = $dbh->prepare("SELECT * FROM usuarios INNER JOIN 	exp ON `usuarios`.id =  WHERE id='".$id."'");
		$select->execute();
		return $select->fetch();
	}

	public function getExp($id){
		global $dbh;
		$select = $dbh->query("SELECT * FROM `exp` INNER JOIN `usuarios` ON `exp`.usuario_id = `usuarios`.id WHERE `usuarios`.id = ".$id);
		$select = $select->fetch(PDO::FETCH_ASSOC);

		$getroutes = $dbh->query("SELECT COUNT(*) as qnt_rotas FROM rotas WHERE usuario_id = ".$id);
		$getroutes = $getroutes->fetch(PDO::FETCH_ASSOC);

		$getqueixas = $dbh->query("SELECT COUNT(*) as qnt_queixas FROM queixas WHERE usuario_id = ".$id);
		$getqueixas = $getqueixas->fetch(PDO::FETCH_ASSOC);

		$getgrupos = $dbh->query("SELECT COUNT(*) as qnt_grupos FROM grupos WHERE aprovado=1 AND usuario_id = ".$id);
		$getgrupos = $getgrupos->fetch(PDO::FETCH_ASSOC);
		return array_merge($select,$getroutes,$getgrupos,$getqueixas);
	}

	public function check($inputarray){ 	
			
		global $dbh;
		$select = $dbh->prepare("SELECT * FROM usuarios WHERE email='".$inputarray['email']."'");
		$select->execute();
		return $select->rowCount();
	}	
	
	public function insert($inputarray){ 
		global $dbh;

		if(!isset($inputarray['gender']))
			$inputarray['gender'] = "";
		if(!isset($inputarray['locale']))
			$inputarray['locale'] = "";
		if(!isset($inputarray['picture']))
		$inputarray['picture'] = "";

		$insert = $dbh->prepare("INSERT INTO `usuarios`(`name`, `email`, `oauth_type`, `gender`, `locale`,`picture`,`created`,`modified`) 
		VALUES (
		'".$inputarray['name']."',
		'".$inputarray['email']."',
		'".$inputarray['oauth_type']."',
		'".$inputarray['gender']."',
		'".$inputarray['locale']."',
		'".$inputarray['picture']."',
		'".date("Y-m-d H:i:s")."',
		'".date("Y-m-d H:i:s")."'
		)");
		$result = $insert->execute();		
		return $this->select($inputarray['email']);
	}
	
	public function update($inputarray){ 		
		global $dbh;

		if(!isset($inputarray['gender']))
			$inputarray['gender'] = "";
		if(!isset($inputarray['locale']))
			$inputarray['locale'] = "";
		if(!isset($inputarray['picture']))
			$inputarray['picture'] = "";

		$this->diffDate($inputarray['email']);

		$update = $dbh->prepare("UPDATE `usuarios` SET 
		`name`='".$inputarray['name']."',
		`email`='".$inputarray['email']."',
		`oauth_type`='".$inputarray['oauth_type']."',
		`gender`='".$inputarray['gender']."' ,
		`locale`='".$inputarray['locale']."' ,
		`picture`='".$inputarray['picture']."', 
		`modified`='".date("Y-m-d H:i:s")."' 
		WHERE email='".$inputarray['email']."'");
		$update->execute();
		return $this->select($inputarray['email']);
	}
	/* END USER FUNCTIONS */
	// ---
	/* BEGIN ISSUES FUNCTIONS */
	public function insereQueixa($tipo, $endereco, $latitude, $longitude, $comentario, $foto, $user_id){
		global $dbh;

		$insert = $dbh->prepare("SET NAMES utf8; INSERT INTO `queixas` (`tipo`, `endereco`, `latitude`, `longitude`, `comentario`,`foto`,`usuario_id`) 
		VALUES (
		'".$tipo."',
		'".$endereco."',
		'".$latitude."',
		'".$longitude."',
		'".$comentario."',
		'".$foto."',
		'".$user_id."'
		)");
		$result = $insert->execute();
		return $result;
	}

	public function selectQueixas(){
		global $dbh;
		// executa a instrução SQL
		$consulta = $dbh->query("SELECT * FROM queixas;");
		$linha = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $linha;
	}

	//value:1 = like, 0 = dislike
	//tipo:1 = likesystem, 0 = ratingsystem
	public function inserControl($user_id, $type, $nota_id, $queixa_id, $value){
		global $dbh;
		$string = "";

		if($type == 1){
			$consult = $dbh->query("SELECT * FROM controle WHERE `usuario_id`=".$user_id." AND `queixa_id`=".$queixa_id.";");
			if(!$consult) return;
			$linha = $consult->fetch(PDO::FETCH_ASSOC);
			
			//check if already exist
			if($linha){
				$value_before = $linha['value'];
				if($value_before == $value)
					return -1;

				if($value == 1){
					$string = "`likes`=`likes`+1,`dislikes`=`dislikes`-1";
				}else{
					$string = "`dislikes`=`dislikes`+1, `likes` = `likes`-1";
				}
				$insert = $dbh->prepare("UPDATE controle SET `value`=".$value." WHERE `usuario_id`=".$user_id." AND `queixa_id`=".$queixa_id.";");
				$insert = $insert->execute();
			}else{	//not exist in control
				if($value == 1){
					$string = "`likes`=`likes`+1";
				}else{
					$string = "`dislikes`=`dislikes`+1";
				}
				$insert = $dbh->query("INSERT INTO controle (`value`,`rota_id`,`usuario_id`,`queixa_id`) 
					VALUES (".$value.",0,".$user_id.",".$queixa_id.");");
			}

			// update no queixas
			$insert = $dbh->prepare("UPDATE queixas SET ".$string." WHERE `idqueixas` = ".$queixa_id.";");
			$result = $insert->execute();
		}

		$consulta = $dbh->query("SELECT likes,dislikes FROM queixas WHERE `idqueixas`=".$queixa_id.";");
		$linha = $consulta->fetch(PDO::FETCH_ASSOC);

		// the user lose 2 points
		if($linha['dislikes'] >= 10)
			$this->updateExp($user_id, -2);

		return $linha;
	}
	/* END ISSUES FUNCTIONS */
	// --
	/* ROUTES FUNCTIONS */
	public function insertRoute($tempo, $inicio, $fim, $category1, $category2, $user_id){
		global $dbh;
		$id = -1;

		// insere rota
		$sql = "INSERT INTO rotas (`tempo`,`inicio`,`fim`,`categoria1`,`categoria2`,`usuario_id`) 
			VALUES ('".$tempo."','".$inicio."','".$fim."','".$category1."','".$category2."','".$user_id."');";
		$insert = $dbh->prepare($sql);
		$result = $insert->execute();
		//insert successull
		if($result){
			//get the rota_id for insert points later
			$id = $dbh->lastInsertId();
		}
		return $id;
	}

	public function selectAllRoutes(){
		global $dbh;
		// executa a instrução SQL
		$consulta = $dbh->query("SELECT * FROM routes;");
		$linha = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $linha;
	}

	public function selectUserRoutes($user_id){
		global $dbh;
		// executa a instrução SQL
		
		$consulta = $dbh->query("SELECT * FROM rotas WHERE `usuario_id` = ".$user_id);
		$linha = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $linha;
	}

	public function selectOtherUserRoutes($user_id){
		global $dbh;
		// executa a instrução SQL
		$sql = "SELECT * FROM rotas WHERE id IN 
			(SELECT rota_id FROM `grupos` WHERE usuario_id = ".$user_id." AND aprovado=1) AND usuario_id != ".$user_id;
		$consulta = $dbh->query($sql);
		$linha = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $linha;
	}

	public function selectRoute($route_id){
		global $dbh;
		// executa a instrução SQL
		$consulta = $dbh->query("SELECT * FROM `rotas` INNER JOIN `usuarios` 
				ON `rotas`.usuario_id = `usuarios`.id WHERE `rotas`.id=".$route_id.";");
		$linha = $consulta->fetch(PDO::FETCH_ASSOC);
		return $linha;
	}

	public function selectRouteRating($route_id){
		global $dbh;
		$consulta = $dbh->query("SELECT `nota`, `qnt_votos` FROM notas WHERE `rota_id` = ".$route_id.";");
		$linha = $consulta->fetch(PDO::FETCH_ASSOC);
		return $linha;
	}
	
	public function updateRouteRating($route_id, $note, $user_id){
		global $dbh;
		$qnt_votos = 0;

		$consult = $dbh->query("SELECT * FROM controle WHERE `usuario_id` = ".$user_id." AND `rota_id`= ".$route_id.";");
		if(!$consult) return -1;
			$linha = $consult->fetch(PDO::FETCH_ASSOC);

		//NOT voted
		if(!$linha){
			$qnt_votos = 1;
			$insert = $dbh->query("INSERT INTO controle (`value`,`rota_id`,`usuario_id`,`queixa_id`) 
					VALUES (".$note.",$route_id,".$user_id.",0);");		
		}else{
			$insert = $dbh->prepare("UPDATE controle SET `value`=".$note." WHERE `usuario_id`=".$user_id." AND `rota_id`=".$route_id.";");
			$insert = $insert->execute();
			$note = $note - $linha['value'];
		}

		$consulta = $dbh->query("SELECT `nota`,`qnt_votos` FROM notas WHERE `rota_id` = ".$route_id.";");
		$linha = $consulta->fetch(PDO::FETCH_ASSOC);
		if($linha['qnt_votos'] > 0){
			$avg_ant = $linha['nota']/$linha['qnt_votos'];
			$ang_now = ($linha['nota']+$note)/($linha['qnt_votos']+1);
			if($avg_ant < 3 && $avg_now >= 3){
				$this->updateExp($user_id, 5);
			}else if($avg_ant >= 3 && $avg_now < 3){
				$this->updateExp($user_id, -5);
			}
		}else if($note < 3){
			$this->updateExp($user_id, -5);
		}

		$update = $dbh->prepare("UPDATE notas SET `nota`=`nota`+".$note.",`qnt_votos`=`qnt_votos`+".$qnt_votos." WHERE `rota_id` = ".$route_id.";");
		$result = $update->execute();
		return $result;
	}

	public function insertCoord($latitude,$longitude,$rota_id){
		global $dbh;

		// insere pontos na rota
		$sql = "INSERT INTO pontos (`latitude`,`longitude`,`rota_id`) 
			VALUES (".$latitude.",".$longitude.",".$rota_id.");";
		$insert = $dbh->prepare($sql);
		$result = $insert->execute();
		return $result;
	}

	public function updateRoute($tempo, $rota_id){
		global $dbh;
		// update no queixas
		$update = $dbh->prepare("UPDATE rotas SET `tempo`=".$tempo." WHERE `id` = ".$rota_id.";");
		$result = $update->execute();
		return $result;
	}

	public function getRoutes(){
		global $dbh;
		$consulta = $dbh->query("SELECT rota_id,AVG(latitude) as latitude,AVG(longitude) as longitude from `pontos` GROUP BY rota_id;");
		$consult = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $consult;
	}

	public function getPath($route_id){
		global $dbh;
		$consulta = $dbh->query("SELECT * FROM pontos WHERE rota_id = ".$route_id);
		$consult = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $consult;
	}

	//type = 0 : person leave route
	//type = 1 : person enter route
	public function updateRouteStatus($route_id, $type, $user_id){
		global $dbh;
		$update = $dbh->prepare("UPDATE grupos SET `ativo`=".$type." WHERE `rota_id`=".$route_id." AND `usuario_id`=".$user_id);
		$result = $update->execute();
		return $result;
	}

	public function checkRouteStatus($route_id){
		global $dbh;
		$consulta = $dbh->query("SELECT COUNT(*) as ativo FROM `grupos` WHERE `rota_id` = ".$route_id." AND ativo=1");
		$consulta = $consulta->fetch(PDO::FETCH_ASSOC);
		return $consulta['ativo'];
	} 
	/* END ROUTES FUNCTIONS */
	// ----
	/* START GROUP FUNCTIONS */
	public function searchPersonGroup($rota_id, $user_id){
		global $dbh;
		$sql = "SELECT * FROM grupos WHERE `usuario_id` = ".$user_id." AND `rota_id`=".$rota_id.";";
		$consulta = $dbh->query($sql);
		$consulta = $consulta->fetch(PDO::FETCH_ASSOC);
		return $consulta;
	}

	public function getAllGroup($route_id){
		global $dbh;
		$sql = "SELECT * FROM grupos WHERE `rota_id`=".$route_id.";";
		$consulta = $dbh->query($sql);
		$consulta = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $consulta;
	}

	public function getGroup($route_id){
		global $dbh;
		$sql = "SELECT * FROM `usuarios` INNER JOIN `grupos` WHERE `usuarios`.id = `grupos`.usuario_id AND `grupos`.rota_id = ".$route_id.";";
		$consulta = $dbh->query($sql);
		$consulta = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $consulta;
	}

	public function addPersonGroup($rota_id, $user_id){
		global $dbh;
		$sql = "INSERT INTO grupos (`usuario_id`,`rota_id`) 
			VALUES (".$user_id.",".$rota_id.");";
		$insert = $dbh->prepare($sql);
		$result = $insert->execute();
		return $result;
	}

	public function approvePersonGroup($rota_id, $user_id){
		global $dbh;
		$sql = "UPDATE grupos SET `aprovado`= 1 WHERE `rota_id` = ".$rota_id." AND `usuario_id` = ".$user_id.";";
		$update = $dbh->prepare($sql);
		$result = $update->execute();
		return $result;
	}

	public function removePersonGroup($rota_id, $user_id){
		$sql = "DELETE FROM grupos WHERE `rota_id` = ".$rota_id." AND `usuario_id` = ".$user_id.";";
		$delete = $dbh->prepare($sql);
		$delete->execute();
	}
	/* END GROUP FUNCTIONS */
	// -----
	/* START MESSAGE FUNCTIONS */
	public function getMessages($route_id){
		global $dbh;
		$consulta = $dbh->query("SELECT u.name as name, m.mensagem as message, TIME_FORMAT(m.created, '%H:%i') as created 
			FROM mensagens m INNER JOIN `usuarios` u 
			on u.id = m.usuario_id 
			WHERE m.rota_id =".$route_id.";");
		$consulta = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $consulta;
	}

	public function insertMessage($user_id, $route_id, $message){
		global $dbh;
		$sql = "INSERT INTO mensagens (`mensagem`, `usuario_id`, `rota_id`) 
			VALUES ('".$message."','".$user_id."','".$route_id."');";
		$insert = $dbh->prepare($sql);
		$result = $insert->execute();
		return $result;
	}

	public function deleteMessage($route_id, $message_id){
		global $dbh;
		$sql = "DELETE FROM mensagens WHERE `rota_id` = ".$rota_id." AND `usuario_id` = ".$user_id.";";
		$delete = $dbh->prepare($sql);
		$delete->execute();
	}
	/* END MESSAGE FUNCTIONS */
	//--
	/* START EXP FUNCTIONS */
	public function updateExp($user_id, $value, $distance = 0){
		global $dbh;
		$total = 0;
		if($value > 0) 
			$total = $value;
		
		$select = $dbh->query("SELECT pontos FROM `exp` WHERE `usuario_id`= '".$user_id."';");		
		$select = $select->fetch(PDO::FETCH_ASSOC);
		if($select['pontos']+$value < 0)
			$value = $select['pontos']*(-1);

		$sql = "UPDATE exp SET `ant_pontos`=`pontos`, `pontos` = `pontos` + ".$value." , `max_pontos` = `max_pontos` + ".$total." ,
		 `distancia` = `distancia`+".$distance." WHERE `usuario_id` = ".$user_id.";";
		$update = $dbh->prepare($sql);
		$update->execute();
	}

	public function diffDate($email){
		global $dbh;
		$select = $dbh->query("SELECT id, modified FROM `usuarios` WHERE `email`= '".$email."';");		
		$select = $select->fetch(PDO::FETCH_ASSOC);

		$now = new DateTime();
		$modified = new DateTime($select['modified']);
		// echo "select = ".$modified;

		$diff = $now->diff($modified);
		$elapsed = $diff->format('%a');
		$days = intval($elapsed);

		if($days > 3){
		 	$this->updateExp($select['id'], $days*(-1));
		}
		return;
	}
}

?>