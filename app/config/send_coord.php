<?php 

session_start();

if(!isset($_POST) || empty($_POST)){
    echo "Error, post data not received!";
}else if(!isset($_POST['latitude']) || !isset($_POST['longitude'])){
    echo "Error, post data not received!"; 
}else{

    if(!isset($_SESSION['distance'])){
        $_SESSION['distance'] = $_POST['distance'];
    }else{
        $_SESSION['distance'] += $_POST['distance'];
    }

    // for bd acess
    require_once('db_op.class.php');
    $database = new db_op();
    
    //inser into database
    $result = $database->insertCoord($_POST['latitude'],$_POST['longitude'],$_SESSION['route']);  
    echo $result;
}
?>