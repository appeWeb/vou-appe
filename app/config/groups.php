<?php

//for db search
require_once('db_op.class.php');
session_start();

if(isset($_POST['user_id']) && isset($_POST['route_id'])){
    $database = new db_op();
    $user_id = $_POST['user_id'];
    $route_id = $_POST['route_id'];
    $result = 'err';

    //0 = request
    //1 = approval
    if($_POST['type'] == 0){
        $result = $database->addPersonGroup($route_id, $user_id);
    }else{
        $result = $database->approvePersonGroup($route_id, $user_id);
        //win 5 points with people in group
        $database->updateExp($_SESSION['user_data']['id'], 5);
        // plus 1 point for approved one
        $database->updateExp($user_id, 1);
    } 
    //Return json formatted rating data
    echo json_encode($result);
}
?>