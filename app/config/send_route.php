<?php 

session_start();
if(!isset($_POST) && !empty($_POST))
    exit(header('Location: ../novarota.php')); 

// for bd acess
require_once('db_op.class.php');
ini_set('default_charset','UTF-8');

// finalizando rota
if(isset($_SESSION['routetime']) && isset($_SESSION["route"]) && isset($_SESSION['distance'])){
    $database = new db_op();
    $result = $database->updateRouteStatus($_SESSION["route"], 0, $user_id);
    $result = $database->updateRoute(ceil(time()-$_SESSION['routetime']), $_SESSION['user_data']['id']);

    //win 10 points because route submission
    $database->updateExp($_SESSION['user_data']['id'], 10, $_SESSION['distance']);
    unset($_SESSION["route"]);
    unset($_SESSION["routetime"]);
    unset($_SESSION["distance"]);
    exit(header('Location: ../index.php?rota_status='.$result));
}

/* new of route creation */
if(!isset($_POST['inicio']) || !isset($_POST['fim'])){
    echo "Por favor, inclua a localização de inicio e fim";
    die();
}else if(!isset($_POST['char']) || count($_POST['char']) != 2 ){
    echo "Por favor, selecione 2 categorias!";
    die();
}

$char1 = $_POST['char'][0];
$char2 = $_POST['char'][1];

$database = new db_op();
$result = $database->insertRoute(0, $_POST['inicio'], $_POST['fim'], $char1, $char2, $_SESSION['user_data']['id']);  
if($result == -1){
    echo "Não foi possível inserior ou receber o id da rota inserida!";
    die();
}
$_SESSION['route'] = $result;
$_SESSION['routetime'] = time();
$_SESSION["distance"] = 0;
exit(header("Location: ../criandorota.php"));
/* end of route creation*/
?>