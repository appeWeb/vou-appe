<?php 

session_start();

if(!isset($_POST) || empty($_POST)){
    echo "Error, post data not received!";
}else if(!isset($_POST['route_id']) || !isset($_POST['user_id']) || !isset($_POST['message'])){
    echo "Error, post data not received!"; 
}else{
    // for bd acess
    require_once('db_op.class.php');
    $database = new db_op();
    
    //inser into database
    $result = $database->insertMessage($_POST['user_id'],$_POST['route_id'],$_POST['message']);  
    echo $result;
}
?>