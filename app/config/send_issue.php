<?php

session_start();
//redirect to login
if(!isset($_SESSION['user_data']['name']))
  exit(header('Location: ../login.php')); 
// not received submit
if(!isset($_POST) && !empty($_POST))
    exit(header('Location: ../novaqueixa.php'));

// for bd acess
require_once('db_op.class.php');
ini_set('default_charset','UTF-8');

$new_file_dir = null;
$exp = 1;

if(isset($_FILES['image']) && !empty($_FILES['image']) && $_FILES['image']['name'] != ""){
    $errors = array();
    

    $target_dir = "../uploads/";
    $file_name = $_FILES['image']['name'];
    $new_file_name = uniqid();
    $file_size = $_FILES['image']['size'];
    $file_tmp = $_FILES['image']['tmp_name'];
    $file_type = $_FILES['image']['type'];
    $file_ext = explode('.',$file_name);
    $file_ext = strtolower(end($file_ext));

    $expensions = array("jpeg","jpg","png","gif");

    // Verify the file
    if(!getimagesize($file_tmp))
        $errors[] = "Arquivo não é uma imagem.";

    // max size = 3Mb
    if($file_size > 3145728)
        $errors[] = "Desculpe, apenas imagens abaixo de 3mb são permitidas.";

    // Allow certain file formats
    if(in_array($file_ext,$expensions) === false)
        $errors[] = "Desculpe, apenas os formatos JPG, JPEG, PNG e GIF são permitidos.";

    if(empty($erros)){
        $new_file_dir = $target_dir.$new_file_name.'.'.$file_ext;
        if(!move_uploaded_file($file_tmp,$new_file_dir))
            $errors[] = "Desculpe, a imagem nao pôde ser enviada, por favor tente mais tarde.";
        else
            $exp += 1;
    }

    if(!empty($errors)){
    	echo "<pry>";
        print_r($errors);
        die();
    }
}

$database = new db_op();
$result = $database->insereQueixa($_POST['issue'], $_SESSION['address'], $_SESSION['latitude'],
    $_SESSION['longitude'], $_POST['problem'], $new_file_dir, $_SESSION['user_data']['id']);
if($result){
    $result = "succeed";        
    //win 1 point with issue send
    $database->updateExp($_SESSION['user_data']['id'], $exp);
}else
    $result = "failed";   


if(isset($_SESSION['route']))
    exit(header("Location: ../criandorota.php"));
else if(isset($_SESSION['route_follow']))
    exit(header("Location: ../seguindorota.php"));
else
    exit(header("Location: ../index.php?send_issue=".$result));

?>